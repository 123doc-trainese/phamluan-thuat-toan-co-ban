<?php
    $words = ["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do"];
    $maxWith = 20;

    $results = [];
    $index = 0;
    $results[0] = $words[0];
    for($i=1; $i<sizeof($words); $i++){                              //chia mang ban dau thanh mang con chua nhieu tu nhat co the
        if(strlen($results[$index].$words[$i])<$maxWith){      //lay ra toi da so tu + 1 dau cach tuong ung dam bao chuoi moi < maxwith
            $results[$index] = $results[$index]." ".$words[$i];
        }else{
            $index++;
            $results[$index] = $words[$i];
        }
    }
    $k = 0;
    for ($k; $k < sizeof($results) -1; $k++){
        if(str_word_count($results[$k]) > 1){
            $space = $maxWith - strlen($results[$k]);           //space la so khoang trang con thieu
            $wordCount = str_word_count($results[$k]);
            if($space < str_word_count($results[$k])-1){        //so khoang trang con thieu < so tu trong chuoi -1, (5 tu thi can 4 khoang trang...)
                $count = $space/2;                              //$count la so khoang trang can chen vao ben trai va ben phai
                if($space % 2 ==0){                             //so khoang trang chia deu 2 ben trai phai
                    for($i = 1; $i <= $count; $i++){
                        $results[$k] = " ".$results[$k]." ";
                    }
                }else{                                          //so khoang trang ben trai nhieu hon ben phai
                    for($i = 1; $i <= $count+1; $i++){
                        $results[$k] = " ".$results[$k];
                    }
                    for($i = 1; $i <= $count; $i++){
                        $results[$k] = $results[$k]." ";
                    }
                }
            }
            else{                                               //so khoang trang con thieu > so ki tu trong chuoi
                $count = $space % ($wordCount -1);
                $pad = ($space - $count)/($wordCount-1);        //pad la so khoang trang can chen vao giua 2 tu bat ki
                for($i = 1; $i <= $pad; $i++){                  //chen so ki tu \ vao giua cac ki tu bang so khoang trang con thieu giua cac tu
                    $results[$k] = addcslashes($results[$k], " ");  //thay the cac ki tu \ bang ki tu khoang trang
                }
                $results[$k] = str_replace("\\", " ", $results[$k]);
                $count = $count/2;
                if($space % 2 ==0){                             //so khoang trang chia deu 2 ben trai phai
                    for($i = 1; $i <= $count; $i++){
                        $results[$k] = " ".$results[$k]." ";
                    }
                }else{                                          //so khoang trang ben trai nhieu hon ben phai
                    for($i = 1; $i <= $count; $i++){
                        $results[$k] = " ".$results[$k];
                    }
                    for($i = 1; $i <= $count; $i++){
                        $results[$k] = $results[$k]." ";
                    }
                }
            }
            echo "\"$results[$k]\"\n";
//            echo strlen($results[$k])."\n";
        }else{
            $results[$k] = str_pad($results[$k],$maxWith," ");
            echo "\"$results[$k]\"\n";
//            echo strlen($results[$k])."\n";
        }
    }
    $results[$k] = str_pad($results[$k],$maxWith," ");   //chuoi cuoi
    echo "\"$results[$k]\"\n";
//    echo strlen($results[$k])."\n";

?>