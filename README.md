# PhamLuan thuật toán cơ bản

***Thuật toán cơ bản***
- Tìm kiếm nhị phân
- Chuỗi con dài nhất không chứa kí tự lặp lại
- Kết hợp chữ cái của một số điện thoại
- Định dạng văn bản

Thực hiện bởi [Phạm Luận](https://facebook.com/phluann)

### Bài 1: tìm kiếm nhị phân
- Cho một mảng các số nguyên nums được sắp xếp theo thứ tự không giảm, 
hãy tìm vị trí bắt đầu và kết thúc của một giá trịtarget nhất định.
Nếu không tìm thấy target trong mảng, hãy trả về[-1, -1].
- điều kiện:
  - 0 <= nums.length <= 105
  -  -109 <= nums [i] <= 109
  -  nums là một mảng không giảm.
  -  -109 <= target <= 109
- bộ test:
```
test 1:
Đầu vào: nums = [5,7,7,8,8,10], target = 8
Đầu ra: [3,4]

test 2:
Đầu vào: nums = [5,7,7,8,8,10], target = 6
Đầu ra: [-1, -1]

test 3:
Đầu vào: nums = [], target = 0
Đầu ra: [-1, -1]
```

- ý tưởng:
  - so sánh target với vị trí giữa mảng (mid = (left+right)/2)
  - vị trí đầu = -1, 
    - nếu target > mid thì tìm ở phần bên phải của mảng: left = mid + 1
    - nếu target < mid thì tìm ở phần bên trái của mảng: right = mid - 1
    - nếu tìm thấy target thì gán vị trí đầu = vị trí của target
      - tìm tiếp từ đầu đến vị trí mid - 1
  - vị trí cuối = -1,
    - nếu target > mid thì tìm ở phần bên phải của mảng: left = mid + 1
    - nếu target < mid thì tìm ở phần bên trái của mảng: right = mid - 1
    - nếu target = mid thì gán vị trí cuối bằng vị trí mid
      - tìm tiếp từ vị trí mid + 1 đến cuối


### Bài 2: chuỗi con dài nhất không chứa kí tự lặp lại
- Cho một chuỗi s, tìm độ dài của chuỗi con dài nhất không có ký tự lặp lại.
- điều kiện
  - 0 <=s.length <= 5 * 104
  - s bao gồm các chữ cái tiếng Anh, chữ số, ký hiệu và dấu cách
- bộ test:
```
test 1:
Đầu vào: s = "abcabcbb"
Đầu ra: 3
Giải thích: Câu trả lời là "abc", với độ dài là 3.

test 2:
Đầu vào: s = "bbbbb"
Đầu ra: 1
Giải thích: Câu trả lời là "b", với độ dài là 1.

test 3:
Đầu vào: s = "pwwkew"
Đầu ra: 3
Giải thích: Câu trả lời là "wke", với độ dài là 3.
Lưu ý rằng câu trả lời phải là một chuỗi con, "pwke" là một dãy con chứ không phải một chuỗi con.
```

- ý tưởng: sử dụng giải thuật sliding window
  - khởi tạo biến `max = 0`, chuỗi `tmp` rỗng để chứa chuỗi con hợp lệ đến thời điểm đang xét
  - lặp từng kí tự từ đầu chuỗi và lưu chuỗi hợp lệ vào biến `tmp`
    - nếu kí tự đang xét chưa xuất hiện trong chuỗi `tmp` thì thêm kí tự đó vào `tmp` và 
    so sánh giá trị `max` với chuỗi hiện tại, nếu `max` đang nhỏ hơn thì gán `max` bằng độ dài chuỗi hiện tại,
  tiếp tục vòng lặp
    - nếu kí tự đang xét đã xuất hiện trong chuỗi `tmp` thì xét đến các chuỗi con tiếp theo của chuỗi ban đầu
  bằng cách loại bỏ đi kí tự đầu trong chuỗi `tmp` và tiếp tục vòng lặp
    - (điều kiện dừng vòng lặp: kí tự cuối cùng của chuỗi ban đầu được xét)

### Bài 3: Kết hợp chữ cái của một số điện thoại
- Cho một chuỗi chứa các chữ số từ 2-9, trả về tất cả các kết hợp chữ cái có thể có mà số đó có thể đại diện. Trả lại câu trả lời theo bất kỳ thứ tự nào.
- (ánh xạ các chữ số sang chữ cái như bàn phím điện thoại)
- điều kiện:
  - 0 <= digits.lenght <= 4
  - digits[i] là một chữ số trong phạm vi ['2', '9'].
- bộ test:
```
test 1:
Đầu vào: chữ số = "23"
Đầu ra: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"]

test 2:
Đầu vào: chữ số = ""
Đầu ra: []

test 3:
Đầu vào: chữ số = "2"
Đầu ra: ["a", "b", "c"]
```

- ý tưởng: 
  - (không xét đến số 0 và 1)
  - nếu nhập vào 1 số: output là ánh xạ các chữ cái của số đó
  - nếu nhập vào 2 số: lần lượt kết hợp các ánh xạ của 2 số
    - lần 1: kết hợp ánh xạ của số đầu với số thứ 2
    - lần 2: kết hợp kết quả bước 1 với ánh xạ của số thứ 3
    - lần 3: kết hợp kết quả bước 2 với ánh xạ của số thứ 4
    - lần n: kết hợp kết quả bước (n-1) với ánh xạ của số (n+1)
  - với n số thì ta cần lặp lại (n-1) lần kết hợp

### Bài 4: Định dạng văn bản
- Cho một mảng các từ words và chiều rộng maxWidth, hãy định dạng văn bản sao cho mỗi dòng có chính xác các maxWidth ký tự và được căn đều (trái và phải).
- Bạn nên gói lời nói của mình theo cách tiếp cận tham lam; nghĩa là, đóng gói càng nhiều từ càng tốt trong mỗi dòng. Thêm khoảng trắng ' ' khi cần thiết để mỗi dòng có chính xác maxWidth ký tự
- Khoảng trống thừa giữa các từ nên được phân bố đồng đều nhất có thể. Nếu số lượng khoảng trắng trên một dòng không chia đều giữa các từ, các ô trống ở bên trái sẽ được gán nhiều khoảng trắng hơn các ô trống ở bên phải
- Đối với dòng cuối cùng của văn bản, nó phải được căn trái và không có khoảng trống thừa nào được chèn giữa các từ.
- ghi chú:
  - Một từ được định nghĩa là một chuỗi ký tự chỉ bao gồm các ký tự không phải khoảng trắng.
  - Độ dài của mỗi từ được đảm bảo lớn hơn 0 và không vượt quá maxWidth.
  - Mảng đầu vào words chứa ít nhất một từ
- điều kiện: 
  - 1 <= words.length <= 300
  -  1 <= words[i].length <= 20
  -  words[i] chỉ bao gồm các chữ cái và ký hiệu tiếng Anh.
  -  1 <= maxWidth <= 100
  -  words[i].length <= maxWidth
- bộ test:
```
test 1:
Input: words = ["This", "is", "an", "example", "of", "text", "justification."], maxWidth = 16
Output:
[
   "This    is    an",
   "example  of text",
   "justification.  "
]

test 2:
Input: words = ["What","must","be","acknowledgment","shall","be"], maxWidth = 16
Output:
[
  "What   must   be",
  "acknowledgment  ",
  "shall be        "
]
Explanation: Note that the last line is "shall be    " instead of "shall     be", because the last line must be left-justified instead of fully-justified.
Note that the second line is also left-justified because it contains only one word.

test 3:
Input: words = ["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do"], maxWidth = 20
Output:
[
  "Science  is  what we",
  "understand      well",
  "enough to explain to",
  "a  computer.  Art is",
  "everything  else  we",
  "do                  "
]
```

- ý tưởng:
  - chia mảng ban đầu thành các mảng con chứa nhiều từ nhất có thể (đã bao gồm một khoảng trắng giữa các từ)
  - với mỗi mảng con,  kiểm tra độ dài của chuỗi được chứa trong mảng:
    - tính số khoảng trắng còn thiếu để chuỗi đạt giá trị maxWith
    - nếu số khoảng trắng còn thiếu >= số từ - 1 (có thể chèn đều vào giữa các từ) thì chèn tối đa các khoảng trắng vào
  vào giữa các từ trong chuỗi
    - nếu số khoảng trắng còn thiếu < số từ -1 (không thể chèn đều các khoảng trắng vào giữa các từ)
  thì chèn vào bên trái và bên phải chuỗi (nếu số lẻ thì chèn nhiều khoảng trắng hơn về phía bên trái)
    - với chuỗi cuối cùng thì sử dụng hàm `str_pad()` để chèn các khoảng trắng còn thiếu vào cuối chuỗi.
