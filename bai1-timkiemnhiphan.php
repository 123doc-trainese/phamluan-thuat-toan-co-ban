<?php
    $arr = [5,7,7,8,8,10];
    $target = 8;    

    function viTriDau($arr, $target, $left, $right){
        static $result = -1;
        while($left <= $right){
            $mid = floor(($left + $right)/2);
            if($arr[$mid] == $target){          //neu target = mid thi duyet tiep tu dau den vi tri mid - 1
                $result = $mid;
                $left = 0;
                $right = $mid - 1;
                return viTriDau($arr, $target, $left, $right);
            }elseif($target > $arr[$mid]){      //neu target > gia tri tai mid thi chi duyet cac phan tu ben phai (mid+1)
                $left = $mid + 1;
                return viTriDau($arr, $target, $left, $right);
            }else{                              // neu target < gia tri tai mid thi chi duyet cac phan tu ben trai (mid-1)
                $right = $mid-1;
                return viTriDau($arr, $target, $left, $right);
            }
        }
        return $result;
    }

    function viTriCuoi($arr, $target, $left, $right){
        static $res = -1;                        //5 7 7 8 8 10
        while($left <= $right){
            $mid = floor(($left + $right)/2);       
            if($arr[$mid] == $target){              //neu target = mid thi duyet tiep vi tri mid + 1 den cuoi
                $res = $mid;
                $left = $mid + 1;
                return viTriCuoi($arr, $target, $left, $right);
            }elseif($target > $arr[$mid]){                //neu target > gia tri tai mid thi chi duyet cac phan tu ben phai (mid+1)
                $left = $mid + 1;
                return viTriCuoi($arr, $target, $left, $right);
            }else{                                  // neu target < gia tri tai mid thi chi duyet cac phan tu ben trai (mid-1)
                $right = $mid-1;
                return viTriCuoi($arr, $target, $left, $right);
            }
        }
        return $res;
    }
    $left = 0;
    $right = count($arr) - 1;
    $result = ['Vi tri dau' => viTriDau($arr, $target, $left, $right), 'vi tri cuoi' => viTriCuoi($arr, $target, $left, $right)];

    print_r($result);
?>