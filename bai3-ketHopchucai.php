<?php
    static $digits = array(
        1=>[],
        2=>['a','b','c'],
        3=>['d','e','f'],
        4=>['g','h','i'],
        5=>['j','k','l'],
        6=>['m','n','o'],
        7=>['p','q','r','s'],
        8=>['t','u','v'],
        9=>['w','x','y','s']);
    while(true){
        $results = [];
        $line =  readline();
        if($line == -1) break;
        $length = strlen($line);
        if($length == 1){       //nếu chỉ nhập 1 số thì output bằng ánh xạ của số vừa nhập
            $results = $digits[$line];
        }else{      //nhập >2 số
            $line = addcslashes($line, '1..9');     //thêm dấu \ trước các kí tự
            $nums = explode("\\", substr($line, 1));    //bỏ đi dấu \ đầu tiên, cắt chuỗi thành mảng các chữ số dựa trên dấu \
            $index = 1;
            $num1 = $digits[$nums[0]];
            $num2 = $digits[$nums[1]];
            while ($index < sizeof($nums)){     //lap n-1 lan
                $results = mapping($num1, $num2);
                $index++;
                if($index < sizeof($nums)){
                    $num1 = $results;
                    $num2 = $digits[$nums[$index]];
                }
            }
        }
        print_out($results);
//        echo sizeof($results);
    }

    /**
     * Hiển thị kết quả
     * @param $array
     * @return void
     */
    function print_out($array){
        echo "[";
        for($i = 0; $i<sizeof($array); $i++){
            echo "\"$array[$i]\"";
            if($i < sizeof($array) -1){
                echo ", ";
            }
        }
        echo "] \n";
    }

    /**
     * kết hợp các kết quả từ 2 mảng
     * @param $arr1 array
     * @param $arr2 array
     * @return array
     */
    function mapping($arr1, $arr2){
        $result = [];
        for($i = 0; $i<sizeof($arr1); $i++){
            for($j = 0; $j<sizeof($arr2); $j++){
                array_push($result, $arr1[$i].$arr2[$j]);
            }
        }
        return $result;
    }
?>